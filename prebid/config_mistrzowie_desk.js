
    var googletag = null;
    var pbjs = null;
    var PREBID_TIMEOUT=1500;
    const GPTScript = document.createElement('script');      
    GPTScript.async = true;      
    GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');      
    top.document.head.appendChild(GPTScript);      
    
    const AAScript = document.createElement('script');      
    AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');      
    AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');      
    top.document.head.appendChild(AAScript);   
    
    //#region initAdserver
    function initAdserver() {
        if (pbjs.initAdserverSet) return;
        pbjs.initAdserverSet = true;
        googletag.cmd.push(function() {
            pbjs.que.push(function() {
                pbjs.setTargetingForGPTAsync();
                googletag.pubads().refresh();
            });
        });
    }
    //#endregion
    
    
    setTimeout(() => {
        var div = document.createElement('div');
        div.style.textAlign = 'center';
        div.style.color = 'grey'
        div.style.fontSize = '10px'
        div.innerHTML = 'Reklama'
        var divClone = div.cloneNode(true);
        if (document.getElementById('billboard') != null){
        document.getElementById('billboard').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('billboard_2') != null){
        document.getElementById('billboard_2').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('billboard_3') != null){
        document.getElementById('billboard_3').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('billboard_4') != null){
        document.getElementById('billboard_4').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('rec_1') != null){
        document.getElementById('rec_1').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('rec_2') != null){
        document.getElementById('rec_2').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('rec_3') != null){
        document.getElementById('rec_3').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('rec_4') != null){
        document.getElementById('rec_4').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if ((document.getElementsByClassName('google-auto-placed')[0]) != null){
        document.getElementsByClassName('google-auto-placed')[0].prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if ((document.getElementsByClassName('google-auto-placed')[1]) != null){
        document.getElementsByClassName('google-auto-placed')[1].prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        
    }, 5000);
    
    window.googletag = window.googletag || {
        cmd: []
    };
    var slot1, slot2, slot3, slot4, slot5, slot6, slot7, slot8, slot9, slot10, slot11, slot12, slot13, slot14, slot15;
    googletag.cmd.push(function() {   
    slot1 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/4417030854', [[300, 600]], 'half_sticky') //half sticky
                    .addService(googletag.pubads());
    slot2 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/5018380974', [
                [750, 300],
                [750, 200]
            ], 'billboard') //billboard
            .addService(googletag.pubads());
    slot3 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_billboard_2', [
                [750, 300]
            ], 'billboard_2') //billboard 2
            .addService(googletag.pubads());
    // slot4 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_billboard_3', [
    //             [750, 300]
    //         ], 'billboard_3') //billboard 3
    //         .addService(googletag.pubads());
    // slot5 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_billboard_4', [
    //             [750, 300]
    //         ], 'billboard_4') //billboard 4
    //         .addService(googletag.pubads());
    slot6 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_300x600_1', [
                [300, 600]
            ], 'half_1') //half 1
            .addService(googletag.pubads());
    slot7 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_300x600_2', [
                [300, 600]
            ], 'half_2') //half 2
            .addService(googletag.pubads());
    slot8 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_300x600_3', [
                [300, 600]
            ], 'half_3') //half 3
            .addService(googletag.pubads());
    slot9 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_300x600_4', [
                [300, 600]
            ], 'half_4') //half 4
            .addService(googletag.pubads());
    slot10 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/9569272854', [
                [336, 280],
                [300, 250]
            ], 'rec_1') //rec_1
            .addService(googletag.pubads());
    slot11 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/6033491454', [
                [336, 280],
                [300, 250]
            ], 'rec_2') //rec_2
            .addService(googletag.pubads());
    slot12 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/8847154494', [
                [336, 280],
                [300, 250]
            ], 'rec_3') //rec_3
            .addService(googletag.pubads());
    slot13 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_4', [
                [336, 280],
                [300, 250]
            ], 'rec_4') //rec_4
            .addService(googletag.pubads());
    // slot14 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_5', [
    //             [336, 280],
    //             [300, 250]
    //         ], 'rec_5') //rec_5
    //         .addService(googletag.pubads());
    // slot15 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_6', [
    //             [336, 280],
    //             [300, 250]
    //         ], 'rec_6') //rec_6
    //         .addService(googletag.pubads());
            
        googletag.pubads().enableSingleRequest();
        googletag.pubads().disableInitialLoad();    
        googletag.pubads().collapseEmptyDivs();
        googletag.pubads().setForceSafeFrame(false);
        googletag.pubads().setCentering(true);
        googletag.enableServices();
    });
    
    googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    
    pbjs = pbjs || {};
    pbjs.que = pbjs.que || [];
    
    pbjs.que.push(function() {
        pbjs.addAdUnits(adUnits);
        pbjs.setConfig({
            "schain":{ "validation": "strict", "config": { "ver":"1.0", "complete": 1, "nodes": [ { "asi":"yieldriser.com", "sid":"1", "hp":1 } ] }},
            bidderSequence: "random",
            consentManagement: {
                cmpApi: 'iab',
                timeout: 5000,
                allowAuctionWithoutConsent: true
            }
        });
        pbjs.requestBids({
            bidsBackHandler: initAdserver,
            timeout: PREBID_TIMEOUT
        });
    });
    
    //half_sticky
    let boxElement_half_sticky;
    let prevRatio_half_sticky = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('half_sticky') != null){
      boxElement_half_sticky = document.getElementById("half_sticky");
      createObserver_half_sticky();
    }}, false);
    function createObserver_half_sticky() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_half_sticky, options);
      observer = observer.observe(boxElement_half_sticky);
    }
    var startTimeoutRefresh_half_sticky;
    var startIntervalRefresh_half_sticky;
    function startInterval_half_sticky() {
        startIntervalRefresh_half_sticky = setInterval(refreshBid_half_sticky, 10000);
        
      }
      
      function stopInterval_half_sticky() {
        clearInterval(startIntervalRefresh_half_sticky);
        
      }
    function handleIntersect_half_sticky(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_half_sticky && !document.hidden) {
          entry.target = startInterval_half_sticky()
        } else {
          entry.target = stopInterval_half_sticky()
        }
        prevRatio_half_sticky = entry.intersectionRatio;
      });
    }
    
    function refreshBid_half_sticky() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['half_sticky'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['half_sticky']);
                    googletag.pubads().refresh([slot1]);
                    googletag.cmd.push(function() {
                        googletag.display('half_sticky');
                    });
                }
            });
        });
    }
    //billboard
    let boxElement_billboard;
    let prevRatio_billboard = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('billboard') != null){
      boxElement_billboard = document.getElementById("billboard");
      createObserver_billboard();
    }}, false);
    function createObserver_billboard() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_billboard, options);
      observer = observer.observe(boxElement_billboard);
    }
    var startTimeoutRefresh_billboard;
    var startIntervalRefresh_billboard;
    function startInterval_billboard() {
        startIntervalRefresh_billboard = setInterval(refreshBid_billboard, 10000);
        
      }
      
      function stopInterval_billboard() {
        clearInterval(startIntervalRefresh_billboard);
        
      }
    function handleIntersect_billboard(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_billboard && !document.hidden) {
          entry.target = startInterval_billboard()
        } else {
          entry.target = stopInterval_billboard()
        }
        prevRatio_billboard = entry.intersectionRatio;
      });
    }
    
    function refreshBid_billboard() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['billboard'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['billboard']);
                    googletag.pubads().refresh([slot2]);
                    googletag.cmd.push(function() {
                        googletag.display('billboard');
                    });
                }
            });
        });
    }
    //billboard_2
    let boxElement_billboard_2;
    let prevRatio_billboard_2 = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('billboard_2') != null){
      boxElement_billboard_2 = document.getElementById("billboard_2");
      createObserver_billboard_2();
    }}, false);
    function createObserver_billboard_2() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_billboard_2, options);
      observer = observer.observe(boxElement_billboard_2);
    }
    var startTimeoutRefresh_billboard_2;
    var startIntervalRefresh_billboard_2;
    function startInterval_billboard_2() {
        startIntervalRefresh_billboard_2 = setInterval(refreshBid_billboard_2, 10000);
        
      }
      
      function stopInterval_billboard_2() {
        clearInterval(startIntervalRefresh_billboard_2);
        
      }
    function handleIntersect_billboard_2(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_billboard_2 && !document.hidden) {
          entry.target = startInterval_billboard_2()
        } else {
          entry.target = stopInterval_billboard_2()
        }
        prevRatio_billboard_2 = entry.intersectionRatio;
      });
    }
    
    function refreshBid_billboard_2() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['billboard_2'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['billboard_2']);
                    googletag.pubads().refresh([slot3]);
                    googletag.cmd.push(function() {
                        googletag.display('billboard_2');
                    });
                }
            });
        });
    }
    
    //half_1
    let boxElement_half_1;
    let prevRatio_half_1 = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('half_1') != null){
      boxElement_half_1 = document.getElementById("half_1");
      createObserver_half_1();
    }}, false);
    function createObserver_half_1() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_half_1, options);
      observer = observer.observe(boxElement_half_1);
    }
    var startTimeoutRefresh_half_1;
    var startIntervalRefresh_half_1;
    function startInterval_half_1() {
        startIntervalRefresh_half_1 = setInterval(refreshBid_half_1, 10000);
       
      }
      
      function stopInterval_half_1() {
        clearInterval(startIntervalRefresh_half_1);
       
      }
    function handleIntersect_half_1(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_half_1 && !document.hidden) {
          entry.target = startInterval_half_1()
        } else {
          entry.target = stopInterval_half_1()
        }
        prevRatio_half_1 = entry.intersectionRatio;
      });
    }
    
    function refreshBid_half_1() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['half_1'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['half_1']);
                    googletag.pubads().refresh([slot6]);
                    googletag.cmd.push(function() {
                        googletag.display('half_1');
                    });
                }
            });
        });
    }
    
    //half_2
    let boxElement_half_2;
    let prevRatio_half_2 = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('half_2') != null){
      boxElement_half_2 = document.getElementById("half_2");
      createObserver_half_2();
    }}, false);
    function createObserver_half_2() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_half_2, options);
      observer = observer.observe(boxElement_half_2);
    }
    var startTimeoutRefresh_half_2;
    var startIntervalRefresh_half_2;
    function startInterval_half_2() {
        startIntervalRefresh_half_2 = setInterval(refreshBid_half_2, 10000);
        
      }
      
      function stopInterval_half_2() {
        clearInterval(startIntervalRefresh_half_2);
       
      }
    function handleIntersect_half_2(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_half_2 && !document.hidden) {
          entry.target = startInterval_half_2()
        } else {
          entry.target = stopInterval_half_2()
        }
        prevRatio_half_2 = entry.intersectionRatio;
      });
    }
    
    function refreshBid_half_2() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['half_2'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['half_2']);
                    googletag.pubads().refresh([slot7]);
                    googletag.cmd.push(function() {
                        googletag.display('half_2');
                    });
                }
            });
        });
    }
    
    //half_3
    let boxElement_half_3;
    let prevRatio_half_3 = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('half_3') != null){
      boxElement_half_3 = document.getElementById("half_3");
      createObserver_half_3();
    }}, false);
    function createObserver_half_3() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_half_3, options);
      observer = observer.observe(boxElement_half_3);
    }
    var startTimeoutRefresh_half_3;
    var startIntervalRefresh_half_3;
    function startInterval_half_3() {
        startIntervalRefresh_half_3 = setInterval(refreshBid_half_3, 10000);
       
      }
      
      function stopInterval_half_3() {
        clearInterval(startIntervalRefresh_half_3);
        
      }
    function handleIntersect_half_3(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_half_3 && !document.hidden) {
          entry.target = startInterval_half_3()
        } else {
          entry.target = stopInterval_half_3()
        }
        prevRatio_half_3 = entry.intersectionRatio;
      });
    }
    
    function refreshBid_half_3() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['half_3'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['half_3']);
                    googletag.pubads().refresh([slot8]);
                    googletag.cmd.push(function() {
                        googletag.display('half_3');
                    });
                }
            });
        });
    }
    
    //half_4
    let boxElement_half_4;
    let prevRatio_half_4 = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('half_4') != null){
      boxElement_half_4 = document.getElementById("half_4");
      createObserver_half_4();
    }}, false);
    function createObserver_half_4() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_half_4, options);
      observer = observer.observe(boxElement_half_4);
    }
    var startTimeoutRefresh_half_4;
    var startIntervalRefresh_half_4;
    function startInterval_half_4() {
        startIntervalRefresh_half_4 = setInterval(refreshBid_half_4, 10000);
        
      }
      
      function stopInterval_half_4() {
        clearInterval(startIntervalRefresh_half_4);
        
      }
    function handleIntersect_half_4(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_half_4 && !document.hidden) {
          entry.target = startInterval_half_4()
        } else {
          entry.target = stopInterval_half_4()
        }
        prevRatio_half_4 = entry.intersectionRatio;
      });
    }
    
    function refreshBid_half_4() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['half_4'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['half_4']);
                    googletag.pubads().refresh([slot9]);
                    googletag.cmd.push(function() {
                        googletag.display('half_4');
                    });
                }
            });
        });
    }
    
    //rec_1
    let boxElement_rec_1;
    let prevRatio_rec_1 = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('rec_1') != null){
      boxElement_rec_1 = document.getElementById("rec_1");
      createObserver_rec_1();
    }}, false);
    function createObserver_rec_1() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_rec_1, options);
      observer = observer.observe(boxElement_rec_1);
    }
    var startTimeoutRefresh_rec_1;
    var startIntervalRefresh_rec_1;
    function startInterval_rec_1() {
        startIntervalRefresh_rec_1 = setInterval(refreshBid_rec_1, 10000);
        
      }
      
      function stopInterval_rec_1() {
        clearInterval(startIntervalRefresh_rec_1);
        
      }
    function handleIntersect_rec_1(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_rec_1 && !document.hidden) {
          entry.target = startInterval_rec_1()
        } else {
          entry.target = stopInterval_rec_1()
        }
        prevRatio_rec_1 = entry.intersectionRatio;
      });
    }
    
    function refreshBid_rec_1() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['rec_1'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['rec_1']);
                    googletag.pubads().refresh([slot10]);
                    googletag.cmd.push(function() {
                        googletag.display('rec_1');
                    });
                }
            });
        });
    }
    
    //rec_2
    let boxElement_rec_2;
    let prevRatio_rec_2 = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('rec_2') != null){
      boxElement_rec_2 = document.getElementById("rec_2");
      createObserver_rec_2();
    }}, false);
    function createObserver_rec_2() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_rec_2, options);
      observer = observer.observe(boxElement_rec_2);
    }
    var startTimeoutRefresh_rec_2;
    var startIntervalRefresh_rec_2;
    function startInterval_rec_2() {
        startIntervalRefresh_rec_2 = setInterval(refreshBid_rec_2, 10000);
        
      }
      
      function stopInterval_rec_2() {
        clearInterval(startIntervalRefresh_rec_2);
        
      }
    function handleIntersect_rec_2(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_rec_2 && !document.hidden) {
          entry.target = startInterval_rec_2()
        } else {
          entry.target = stopInterval_rec_2()
        }
        prevRatio_rec_2 = entry.intersectionRatio;
      });
    }
    
    function refreshBid_rec_2() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['rec_2'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['rec_2']);
                    googletag.pubads().refresh([slot11]);
                    googletag.cmd.push(function() {
                        googletag.display('rec_2');
                    });
                }
            });
        });
    }
    
    //rec_3
    let boxElement_rec_3;
    let prevRatio_rec_3 = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('rec_3') != null){
      boxElement_rec_3 = document.getElementById("rec_3");
      createObserver_rec_3();
    }}, false);
    function createObserver_rec_3() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_rec_3, options);
      observer = observer.observe(boxElement_rec_3);
    }
    var startTimeoutRefresh_rec_3;
    var startIntervalRefresh_rec_3;
    function startInterval_rec_3() {
        startIntervalRefresh_rec_3 = setInterval(refreshBid_rec_3, 10000);
       
      }
      
      function stopInterval_rec_3() {
        clearInterval(startIntervalRefresh_rec_3);
      
      }
    function handleIntersect_rec_3(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_rec_3 && !document.hidden) {
          entry.target = startInterval_rec_3()
        } else {
          entry.target = stopInterval_rec_3()
        }
        prevRatio_rec_3 = entry.intersectionRatio;
      });
    }
    
    function refreshBid_rec_3() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['rec_3'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['rec_3']);
                    googletag.pubads().refresh([slot12]);
                    googletag.cmd.push(function() {
                        googletag.display('rec_3');
                    });
                }
            });
        });
    }
    
    //rec_4
    let boxElement_rec_4;
    let prevRatio_rec_4 = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('rec_4') != null){
      boxElement_rec_4 = document.getElementById("rec_4");
      createObserver_rec_4();
    }}, false);
    function createObserver_rec_4() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_rec_4, options);
      observer = observer.observe(boxElement_rec_4);
    }
    var startTimeoutRefresh_rec_4;
    var startIntervalRefresh_rec_4;
    function startInterval_rec_4() {
        startIntervalRefresh_rec_4 = setInterval(refreshBid_rec_4, 10000);
       
      }
      
      function stopInterval_rec_4() {
        clearInterval(startIntervalRefresh_rec_4);
        
      }
    function handleIntersect_rec_4(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_rec_4 && !document.hidden) {
          entry.target = startInterval_rec_4()
        } else {
          entry.target = stopInterval_rec_4()
        }
        prevRatio_rec_4 = entry.intersectionRatio;
      });
    }
    
    function refreshBid_rec_4() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['rec_4'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['rec_4']);
                    googletag.pubads().refresh([slot13]);
                    googletag.cmd.push(function() {
                        googletag.display('rec_4');
                    });
                }
            });
        });
    }    
    
    setInterval(checkIfDocIsHidden, 2000);
    function checkIfDocIsHidden(){
    if(document.hidden){    
        stopInterval_half_sticky();
        stopInterval_billboard();
        stopInterval_billboard_2();
        stopInterval_half_1();
        stopInterval_half_2();
        stopInterval_half_3();    
        stopInterval_half_4();
        stopInterval_rec_1();
        stopInterval_rec_2();
        stopInterval_rec_3();    
        stopInterval_rec_4();
    }}
    
    // googletag.cmd.push(function() { googletag.display('billboard'); });
    
    var adUnits=[
        {
            code: 'billboard',
            mediaTypes: {
                banner: {
                    sizes: [[750, 300]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '88430'
                }
            }
            ,
            {
            bidder: 'adform',
                params: {
                    mid: '565946'
                }
            }
            ,
            {
            bidder: 'adform',
                params: {
                    mid: '714497'
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu',
                    publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 145384, pageId: 767412, formatId: 51751,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId: 162117, pageId: 930021, formatId: 54146,
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104353'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
            bidder: 'connectad',
            params: {
                   networkId: '10047',
                   siteId: '1033590'
                        }
                       }
            ,
            {
                bidder: 'ix',
                    params: {
                    siteId: '477688',
                    size: [750, 300]
                           }
                       }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624188',
                        sizes:'41',
                           }
                       }
            ,
            {
                bidder: "amx",
                params: {
                        memberId: "152media"
                        }
                      }
            ,
            {
                bidder: 'selectmedia',
                params: {
                        aid: 606504
                        }
                     }
            ,
            {
                bidder: 'adagio',
                params:{
                        organizationId: '1120',
                        site: 'mistrzowie-org',
                        adUnitElementId: 'billboard',
                        placement: 'billboard',
                        environment: 'desktop',
                        }
                    }
            ,
            {
                bidder: 'sspBC'
                    }                  
        ]
        }
        ,
        {
            code: 'billboard_2',
            mediaTypes: {
                banner: {
                    sizes: [[750, 300]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '956084'
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu',
                    publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 145384, pageId: 767412, formatId: 51751,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId: 162117, pageId: 930021, formatId: 54146,
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104353'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                   networkId: '10047',
                   siteId: '1033590'
                        }
                       }
            ,
            {
                bidder: 'ix',
                    params: {
                    siteId: '477688',
                    size: [750, 300]
                           }
                       }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624188',
                        sizes:'41',
                           }
                       }
            ,
            {
                bidder: "amx",
                    params: {
                          memberId: "152media"
                        }
                      }
            ,
            {
                bidder: 'selectmedia',
                    params: {
                        aid: 606504
                        }
                    }  
        ,
        {
            bidder: 'adagio',
            params:{
                organizationId: '1120',
                site: 'mistrzowie-org',
                adUnitElementId: 'billboard_2',
                placement: 'billboard_2',
                environment: 'desktop',
            }
        }  
        ,
        {
                bidder: 'sspBC'
                }                               
        ]
        }
        ,        
        {
            code: 'half_sticky',
            mediaTypes: {
                banner: {
                    sizes: [[300, 600]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '114466'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    "networkId": 2422,
                    "siteId": 145384,
                    "pageId": 818285,
                    "formatId": 65479
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    "networkId": 2581, "siteId": 162117, "pageId": 1196412, "formatId": 55370
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104355'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                    networkId: '10047', siteId: '1033590'
                }
            }
            ,
            {
                bidder: 'ix',
                params: {
                    siteId: '477688', size: [300, 600]
                }
            }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624190',
                        sizes:'10',
                           }
            }
            ,
            {
                bidder: "amx",
                    params: {
                          memberId: "152media"
                        }
            }
            ,
            {
                bidder: 'selectmedia',
                    params: {
                        aid: 606504
                        }
        } 
        ,
        {
                bidder: 'adagio',
                params:{
                organizationId: '1120',
                site: 'mistrzowie-org',
                adUnitElementId: 'half_sticky',
                placement: 'half_sticky',
                environment: 'desktop',
            }
        } 
        ,
        {
                bidder: 'sspBC'
                }           
        ]
        }
        ,
        {
            code: 'half_1',
            mediaTypes: {
                banner: {
                    sizes: [[300, 600]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '956104'
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 145384, pageId: 818285, formatId: 65479,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId: 162117, pageId: 1196412, formatId: 55370,
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104355'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                    networkId: '10047', siteId: '1033590'
                }
            }
            ,
            {
                bidder: 'ix',
                params: {
                    siteId: '477688', size: [300, 600]
                }
            }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624190',
                        sizes:'10',
                           }
            }
            ,
            {
                bidder: "amx",
                    params: {
                          memberId: "152media"
                        }
            }
            ,
            {
                bidder: 'selectmedia',
                    params: {
                        aid: 606504
                        }
        }  
        ,
        {
            bidder: 'adagio',
            params:{
                organizationId: '1120',
                site: 'mistrzowie-org',
                adUnitElementId: 'half_1',
                placement: 'half_1',
                environment: 'desktop',
            }
        } 
        ,
        {
                bidder: 'sspBC'
                }                  
            ]
        }
        ,
        {
            code: 'half_2',
            mediaTypes: {
                banner: {
                    sizes: [[300, 600]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '956105'
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 145384, pageId: 818285, formatId: 65479,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId: 162117, pageId: 1196412, formatId: 55370,
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104355'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                    networkId: '10047', siteId: '1033590'
                }
            }
            ,
            {
                bidder: 'ix',
                params: {
                    siteId: '477688', size: [300, 600]
                }
            }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624190',
                        sizes:'10',
                        }
            }
            ,
            {
                bidder: "amx",
                    params: {
                          memberId: "152media"
                        }
            }
            ,
            {
                bidder: 'selectmedia',
                    params: {
                        aid: 606504
                        }
        }  
        ,
        {
            bidder: 'adagio',
            params:{
                organizationId: '1120',
                site: 'mistrzowie-org',
                adUnitElementId: 'half_2',
                placement: 'half_2',
                environment: 'desktop',
            }
        }    
        ,
        {
                bidder: 'sspBC'
                }               
            ]
        }
        ,
        {
            code: 'half_3',
            mediaTypes: {
                banner: {
                    sizes: [[300, 600]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '956106'
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 145384, pageId: 818285, formatId: 65479,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId: 162117, pageId: 1196412, formatId: 55370,
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104355'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                    networkId: '10047', siteId: '1033590'
                }
            }
            ,
            {
                bidder: 'ix',
                params: {
                    siteId: '477688', size: [300, 600]
                }
            }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624190',
                        sizes:'10',
                        }
            }
            ,
            {
                bidder: "amx",
                    params: {
                          memberId: "152media"
                        }
            }
            ,
            {
                bidder: 'selectmedia',
                    params: {
                        aid: 606504
                        }
        }   
        ,
        {
            bidder: 'adagio',
            params:{
                organizationId: '1120',
                site: 'mistrzowie-org',
                adUnitElementId: 'half_3',
                placement: 'half_3',
                environment: 'desktop',
            }
        }  
        ,
        {
                bidder: 'sspBC'
                }                     
            ]
        }
        ,
        {
            code: 'half_4',
            mediaTypes: {
                banner: {
                    sizes: [[300, 600]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '956107'
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 145384, pageId: 818285, formatId: 65479,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId: 162117, pageId: 1196412, formatId: 55370,
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104355'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                    networkId: '10047', siteId: '1033590'
                }
            }
            ,
            {
                bidder: 'ix',
                params: {
                    siteId: '477688', size: [300, 600]
                }
            }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624190',
                        sizes:'10',
                        }
            }
            ,
            {
                bidder: "amx",
                    params: {
                        memberId: "152media"
                        }
            }
            ,
            {
                bidder: 'selectmedia',
                    params: {
                        aid: 606504
                        }
        }  
        ,
        {
            bidder: 'adagio',
            params:{
                organizationId: '1120',
                site: 'mistrzowie-org',
                adUnitElementId: 'half_4',
                placement: 'half_4',
                environment: 'desktop',
            }
        }  
        ,
        {
                bidder: 'sspBC'
                }                 
            ]
        }
        ,
        {
            code: 'rec_1',
            mediaTypes: {
                banner: {
                    sizes: [[336, 280], [300, 250]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '179562'
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 145384, pageId: 770176, formatId: 52161,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId: 162117, pageId: 996330, formatId: 54226,
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104354'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                    networkId: '10047', siteId: '1033590'
                }
            }
            ,
            {
                bidder: 'ix',
                params: {
                    siteId: '477688', size: [300, 250]
                }
            }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624184',
                        sizes:'16',
                        }
            }
            ,
            {
                bidder: "amx",
                    params: {
                        memberId: "152media"
                        }
            }
            ,
            {
                bidder: 'selectmedia',
                    params: {
                        aid: 606504
                    }
        }  
        ,
        {
            bidder: 'adagio',
            params:{
                organizationId: '1120',
                site: 'mistrzowie-org',
                adUnitElementId: 'rec_1',
                placement: 'rec_1',
                environment: 'desktop',
            }
        }   
        ,
        {
                bidder: 'sspBC'
                }                 
            ]
        }
        ,
        {
            code: 'rec_2',
            mediaTypes: {
                banner: {
                    sizes: [[336, 280], [300, 250]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '179562'
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 145384, pageId: 770176, formatId: 52161,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId: 162117, pageId: 996330, formatId: 54226,
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104354'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                    networkId: '10047', siteId: '1033590'
                }
            }
            ,
            {
                bidder: 'ix',
                params: {
                    siteId: '477688', size: [300, 250]
                }
            }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624184',
                        sizes:'16',
                        }
            }
            ,
            {
                bidder: "amx",
                    params: {
                          memberId: "152media"
                        }
            }
            ,
            {
                bidder: 'selectmedia',
                    params: {
                        aid: 606504
                        }
        }  
        ,
        {
            bidder: 'adagio',
            params:{
                organizationId: '1120',
                site: 'mistrzowie-org',
                adUnitElementId: 'rec_2',
                placement: 'rec_2',
                environment: 'desktop',
            }
        }  
        ,
        {
                bidder: 'sspBC'
                }                 
            ]
        }
        ,
        {
            code: 'rec_3',
            mediaTypes: {
                banner: {
                    sizes: [[336, 280], [300, 250]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '241023'
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 145384, pageId: 770177, formatId: 52161,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId: 162117, pageId: 996330, formatId: 54226,
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104354'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                    networkId: '10047', siteId: '1033590'
                }
            }
            ,
            {
                bidder: 'ix',
                params: {
                    siteId: '477688', size: [300, 250]
                }
            }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624184',
                        sizes:'16',
                        }
            }
            ,
            {
                bidder: "amx",
                    params: {
                          memberId: "152media"
                        }
            }
            ,
            {
                bidder: 'selectmedia',
                    params: {
                        aid: 606504
                        }
        } 
        ,
        {
            bidder: 'adagio',
            params:{
                organizationId: '1120',
                site: 'mistrzowie-org',
                adUnitElementId: 'rec_3',
                placement: 'rec_3',
                environment: 'desktop',
            }
        }  
        ,
        {
                bidder: 'sspBC'
                }                   
            ]
        }
        ,
        {
            code: 'rec_4',
            mediaTypes: {
                banner: {
                    sizes: [[336, 280], [300, 250]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '241023'
                }
            }
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 145384, pageId: 770177, formatId: 52161,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId: 162117, pageId: 996330, formatId: 54226,
                }
            }    
            ,
            {
                bidder: 'emx_digital',
                params: {
                    tagid: '104354'
                }
            }      
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 21068619
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    networkId: 7049
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                    networkId: '10047', siteId: '1033590'
                }
            }
            ,
            {
                bidder: 'ix',
                params: {
                    siteId: '477688', size: [300, 250]
                }
            }
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '316854',
                        zoneId:'1624184',
                        sizes:'16',
                        }
            }
            ,
            {
                bidder: "amx",
                    params: {
                          memberId: "152media"
                        }
            }
            ,
            {
                bidder: 'selectmedia',
                    params: {
                        aid: 606504
          }
        }  
        ,
        {
            bidder: 'adagio',
            params:{
                organizationId: '1120',
                site: 'mistrzowie-org',
                adUnitElementId: 'rec_4',
                placement: 'rec_4',
                environment: 'desktop',
            }
        }   
        ,
        {
                bidder: 'sspBC'
                }                 
            ]
        }    
        ];
    var pbjs=pbjs || {};
    pbjs.que=pbjs.que || [];
    var USD=3.80;
    var EUR=4.50;
    pbjs.bidderSettings= {
        criteo: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*EUR;
            }
        }
        ,
        adform: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.9)*EUR;
            }
        }
        ,
        smartadserver: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm;
            }
        }
        ,
        rtbhouse: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        pulsepoint: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        emx_digital: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        onedisplay: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.85)*EUR;
            }
        }
        ,
        oftmedia: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.85)*USD;
            }
        }
        ,
        imonomy: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        rubicon: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.75)*USD;
            }
        }
        ,
        ix: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        connectad: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        amx: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        sspBC: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm;
            }
        }
        ,
        standard: {
            adserverTargeting: [ {
                key: "hb_bidder",
                val: function (bidResponse) {
                    return bidResponse.bidderCode;
                }
            }
            ,
            {
                key: "hb_adid",
                val: function (bidResponse) {
                    return bidResponse.adId;
                }
            }
            ,
            {
                key: "hb_pb",
                val: function(bidResponse) {
                    var cpm=bidResponse.cpm;
                    if (cpm < 10.00) {
                        return (Math.floor(cpm * 100) / 100).toFixed(2);
                    }
                    else {
                        return '10.00';
                    }
                }
            }
            ]
        }
    };

