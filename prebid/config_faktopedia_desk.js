var googletag = null;
var pbjs = null;

var PREBID_TIMEOUT=3000;

const GPTScript = document.createElement('script');      
GPTScript.async = true;      
GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');      
top.document.head.appendChild(GPTScript);      

const AAScript = document.createElement('script');      
AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');      
AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');      
top.document.head.appendChild(AAScript);   

//#region initAdserver
function initAdserver() {
    if (pbjs.initAdserverSet) return;
    pbjs.initAdserverSet = true;
    googletag.cmd.push(function() {
        pbjs.que.push(function() {
            pbjs.setTargetingForGPTAsync();
            googletag.pubads().refresh();
        });
    });
}
//#endregion


setTimeout(() => {
    var div = document.createElement('div');
    div.style.textAlign = 'center';
    div.style.color = 'grey'
    div.style.fontSize = '10px'
    div.innerHTML = 'Reklama'
    var divClone = div.cloneNode(true);
    if (document.getElementById('top_bill') != null){
    document.getElementById('top_bill').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if (document.getElementById('skyLeft') != null){
    document.getElementById('skyLeft').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if (document.getElementById('skyRight') != null){
    document.getElementById('skyRight').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if (document.getElementById('middle_rect') != null){
    document.getElementById('middle_rect').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if (document.getElementById('top_rect') != null){
    document.getElementById('top_rect').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[0]) != null){
    document.getElementsByClassName('google-auto-placed')[0].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[1]) != null){
    document.getElementsByClassName('google-auto-placed')[1].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[2]) != null){
    document.getElementsByClassName('google-auto-placed')[2].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[3]) != null){
    document.getElementsByClassName('google-auto-placed')[3].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[4]) != null){
    document.getElementsByClassName('google-auto-placed')[4].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[5]) != null){
    document.getElementsByClassName('google-auto-placed')[5].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[6]) != null){
    document.getElementsByClassName('google-auto-placed')[6].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
  

}, 5000);

window.googletag = window.googletag || {
    cmd: []
};
var slot1, slot2, slot3, slot4, slot5;
googletag.cmd.push(function() {   
slot1 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/4179864654', [[750, 200]], 'top_bill_inner') //top_bill_inner
        .addService(googletag.pubads());
slot2 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/2516318694', [[160, 600]], 'sky_left_inner') //sky_left_inner
        .addService(googletag.pubads());
slot3 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/3993026574', [[300, 600]], 'sky_right_inner') //sky_right_inner
        .addService(googletag.pubads());
slot4 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/2781961614', [[300, 250],[336, 280]], 'middle_rect_inner') //middle_rect_inner
        .addService(googletag.pubads());
slot5 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/1305253734', [[300, 250],[336, 280]], 'top_rect_inner') //top_rect_inner
        .addService(googletag.pubads());
        
    googletag.pubads().enableSingleRequest();
    googletag.pubads().disableInitialLoad();    
    googletag.pubads().collapseEmptyDivs();
    googletag.pubads().setForceSafeFrame(false);
    googletag.pubads().setCentering(true);
    googletag.enableServices();
});

googletag = googletag || {};
googletag.cmd = googletag.cmd || [];

pbjs = pbjs || {};
pbjs.que = pbjs.que || [];

pbjs.que.push(function() {
    pbjs.addAdUnits(adUnits);
    pbjs.setConfig({
        "schain":{ "validation": "strict", "config": { "ver":"1.0", "complete": 1, "nodes": [ { "asi":"yieldriser.com", "sid":"28", "hp":1 } ] }},
        bidderSequence: "random",
        disableAjaxTimeout: true,
        consentManagement: {
            cmpApi: 'iab',
            timeout: 5000,
            allowAuctionWithoutConsent: true
        }
    });
    pbjs.requestBids({
        bidsBackHandler: initAdserver,
        timeout: PREBID_TIMEOUT
    });
});

//top_bill_inner
let boxElement_top_bill_inner;
let prevRatio_top_bill_inner = 0.0;
window.addEventListener("load", (event) => {
    if (document.getElementById('top_bill_inner') != null){
  boxElement_top_bill_inner = document.getElementById("top_bill_inner");
  createObserver_top_bill_inner();
}}, false);
function createObserver_top_bill_inner() {
  let observer;
  let options = {
    root: null,
    rootMargin: "10%",
    threshold: 1.0
  };
  observer = new IntersectionObserver(handleIntersect_top_bill_inner, options);
  observer = observer.observe(boxElement_top_bill_inner);
}
var startTimeoutRefresh_top_bill_inner;
var startIntervalRefresh_top_bill_inner;
function startInterval_top_bill_inner() {
    startIntervalRefresh_top_bill_inner = setInterval(refreshBid_top_bill_inner, 10000);
    
  }
  
  function stopInterval_top_bill_inner() {
    clearInterval(startIntervalRefresh_top_bill_inner);
    
  }
function handleIntersect_top_bill_inner(entries, observer) {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > prevRatio_top_bill_inner && !document.hidden) {
      entry.target = startInterval_top_bill_inner()
    } else {
      entry.target = stopInterval_top_bill_inner()
    }
    prevRatio_top_bill_inner = entry.intersectionRatio;
  });
}

function refreshBid_top_bill_inner() {
    console.log("refreshAdUnit")
    pbjs.que.push(function() {
        pbjs.requestBids({
            timeout: PREBID_TIMEOUT,
            adUnitCodes: ['top_bill_inner'],
            bidsBackHandler: function() {
                pbjs.setTargetingForGPTAsync(['top_bill_inner']);
                googletag.pubads().refresh([slot1]);
                googletag.cmd.push(function() {
                    googletag.display('top_bill_inner');
                });
            }
        });
    });
}


//sky_left_inner
let boxElement_sky_left_inner;
let prevRatio_sky_left_inner = 0.0;
window.addEventListener("load", (event) => {
    if (document.getElementById('sky_left_inner') != null){
  boxElement_sky_left_inner = document.getElementById("sky_left_inner");
  createObserver_sky_left_inner();
}}, false);
function createObserver_sky_left_inner() {
  let observer;
  let options = {
    root: null,
    rootMargin: "10%",
    threshold: 1.0
  };
  observer = new IntersectionObserver(handleIntersect_sky_left_inner, options);
  observer = observer.observe(boxElement_sky_left_inner);
}
var startTimeoutRefresh_sky_left_inner;
var startIntervalRefresh_sky_left_inner;
function startInterval_sky_left_inner() {
    startIntervalRefresh_sky_left_inner = setInterval(refreshBid_sky_left_inner, 10000);
    
  }
  
  function stopInterval_sky_left_inner() {
    clearInterval(startIntervalRefresh_sky_left_inner);
    
  }
function handleIntersect_sky_left_inner(entries, observer) {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > prevRatio_sky_left_inner && !document.hidden) {
      entry.target = startInterval_sky_left_inner()
    } else {
      entry.target = stopInterval_sky_left_inner()
    }
    prevRatio_sky_left_inner = entry.intersectionRatio;
  });
}

function refreshBid_sky_left_inner() {
    console.log("refreshAdUnit")
    pbjs.que.push(function() {
        pbjs.requestBids({
            timeout: PREBID_TIMEOUT,
            adUnitCodes: ['sky_left_inner'],
            bidsBackHandler: function() {
                pbjs.setTargetingForGPTAsync(['sky_left_inner']);
                googletag.pubads().refresh([slot2]);
                googletag.cmd.push(function() {
                    googletag.display('sky_left_inner');
                });
            }
        });
    });
}

//sky_right_inner
let boxElement_sky_right_inner;
let prevRatio_sky_right_inner = 0.0;
window.addEventListener("load", (event) => {
    if (document.getElementById('sky_right_inner') != null){
  boxElement_sky_right_inner = document.getElementById("sky_right_inner");
  createObserver_sky_right_inner();
}}, false);
function createObserver_sky_right_inner() {
  let observer;
  let options = {
    root: null,
    rootMargin: "10%",
    threshold: 1.0
  };
  observer = new IntersectionObserver(handleIntersect_sky_right_inner, options);
  observer = observer.observe(boxElement_sky_right_inner);
}
var startTimeoutRefresh_sky_right_inner;
var startIntervalRefresh_sky_right_inner;
function startInterval_sky_right_inner() {
    startIntervalRefresh_sky_right_inner = setInterval(refreshBid_sky_right_inner, 10000);
    
  }
  
  function stopInterval_sky_right_inner() {
    clearInterval(startIntervalRefresh_sky_right_inner);
    
  }
function handleIntersect_sky_right_inner(entries, observer) {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > prevRatio_sky_right_inner && !document.hidden) {
      entry.target = startInterval_sky_right_inner()
    } else {
      entry.target = stopInterval_sky_right_inner()
    }
    prevRatio_sky_right_inner = entry.intersectionRatio;
  });
}

function refreshBid_sky_right_inner() {
    console.log("refreshAdUnit")
    pbjs.que.push(function() {
        pbjs.requestBids({
            timeout: PREBID_TIMEOUT,
            adUnitCodes: ['sky_right_inner'],
            bidsBackHandler: function() {
                pbjs.setTargetingForGPTAsync(['sky_right_inner']);
                googletag.pubads().refresh([slot3]);
                googletag.cmd.push(function() {
                    googletag.display('sky_right_inner');
                });
            }
        });
    });
}

//middle_rect_inner
let boxElement_middle_rect_inner;
let prevRatio_middle_rect_inner = 0.0;
window.addEventListener("load", (event) => {
    if (document.getElementById('middle_rect_inner') != null){
  boxElement_middle_rect_inner = document.getElementById("middle_rect_inner");
  createObserver_middle_rect_inner();
}}, false);
function createObserver_middle_rect_inner() {
  let observer;
  let options = {
    root: null,
    rootMargin: "10%",
    threshold: 1.0
  };
  observer = new IntersectionObserver(handleIntersect_middle_rect_inner, options);
  observer = observer.observe(boxElement_middle_rect_inner);
}
var startTimeoutRefresh_middle_rect_inner;
var startIntervalRefresh_middle_rect_inner;
function startInterval_middle_rect_inner() {
    startIntervalRefresh_middle_rect_inner = setInterval(refreshBid_middle_rect_inner, 10000);
   
  }
  
  function stopInterval_middle_rect_inner() {
    clearInterval(startIntervalRefresh_middle_rect_inner);
   
  }
function handleIntersect_middle_rect_inner(entries, observer) {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > prevRatio_middle_rect_inner && !document.hidden) {
      entry.target = startInterval_middle_rect_inner()
    } else {
      entry.target = stopInterval_middle_rect_inner()
    }
    prevRatio_middle_rect_inner = entry.intersectionRatio;
  });
}

function refreshBid_middle_rect_inner() {
    console.log("refreshAdUnit")
    pbjs.que.push(function() {
        pbjs.requestBids({
            timeout: PREBID_TIMEOUT,
            adUnitCodes: ['middle_rect_inner'],
            bidsBackHandler: function() {
                pbjs.setTargetingForGPTAsync(['middle_rect_inner']);
                googletag.pubads().refresh([slot4]);
                googletag.cmd.push(function() {
                    googletag.display('middle_rect_inner');
                });
            }
        });
    });
}

//top_rect_inner
let boxElement_top_rect_inner;
let prevRatio_top_rect_inner = 0.0;
window.addEventListener("load", (event) => {
    if (document.getElementById('top_rect_inner') != null){
  boxElement_top_rect_inner = document.getElementById("top_rect_inner");
  createObserver_top_rect_inner();
}}, false);
function createObserver_top_rect_inner() {
  let observer;
  let options = {
    root: null,
    rootMargin: "10%",
    threshold: 1.0
  };
  observer = new IntersectionObserver(handleIntersect_top_rect_inner, options);
  observer = observer.observe(boxElement_top_rect_inner);
}
var startTimeoutRefresh_top_rect_inner;
var startIntervalRefresh_top_rect_inner;
function startInterval_top_rect_inner() {
    startIntervalRefresh_top_rect_inner = setInterval(refreshBid_top_rect_inner, 10000);
    
  }
  
  function stopInterval_top_rect_inner() {
    clearInterval(startIntervalRefresh_top_rect_inner);
   
  }
function handleIntersect_top_rect_inner(entries, observer) {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > prevRatio_top_rect_inner && !document.hidden) {
      entry.target = startInterval_top_rect_inner()
    } else {
      entry.target = stopInterval_top_rect_inner()
    }
    prevRatio_top_rect_inner = entry.intersectionRatio;
  });
}

function refreshBid_top_rect_inner() {
    console.log("refreshAdUnit")
    pbjs.que.push(function() {
        pbjs.requestBids({
            timeout: PREBID_TIMEOUT,
            adUnitCodes: ['top_rect_inner'],
            bidsBackHandler: function() {
                pbjs.setTargetingForGPTAsync(['top_rect_inner']);
                googletag.pubads().refresh([slot5]);
                googletag.cmd.push(function() {
                    googletag.display('top_rect_inner');
                });
            }
        });
    });
}



setInterval(checkIfDocIsHidden, 2000);
function checkIfDocIsHidden(){
if(document.hidden){    
    stopInterval_top_bill_inner();
    stopInterval_sky_left_inner();
    stopInterval_sky_right_inner();
    stopInterval_middle_rect_inner();
    stopInterval_top_rect_inner();

}}

//biders

var adUnits=[
    {
        code: 'top_bill_inner',
        mediaTypes: {
            banner: {
                sizes: [[750, 200]],
            }
        }
        ,
        bids: [ 
        {
            bidder: 'adform',
            params: {
                mid: '88459'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId: 150872, pageId: 780352, formatId: 51751,
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2581, siteId: 461666, pageId: 1451620, formatId: 56391,
            }
        }
        ,
        {
            bidder: 'sspBC'
        }  
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '386578',
                    zoneId:'2153588',
                    sizes:'40',
            }
        }   
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1418179,
                publisherSubId:'top_bill_inner'
            }
        } 
        ,
    
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }  
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 22783799
            }
        }  
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1053218'
            }
        }   
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'faktopedia-pl',
                    adUnitElementId: 'top_bill_inner',
                    placement: 'top_bill_inner',
                    environment: 'desktop',
                    }
                }
    ]
    }
    ,
    {
        code: 'sky_left_inner',
        mediaTypes: {
            banner: {
                sizes: [[160, 600]],
            }
        }
        ,
        bids: [ {
            bidder: 'adform',
            params: {
                mid: '348246'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId: 150872, pageId: 780962, formatId: 53517,
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2581, siteId: 461666, pageId: 1451617, formatId: 55373,
            }
        }
        ,
        {
            bidder: 'sspBC'
        } 
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '386578',
                    zoneId:'2153590',
                    sizes:'9',
            }
        } 
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1421652,
                publisherSubId:'sky_left_inner'
            }
        }  
        ,
    
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }  
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 22783799
            }
        }  
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1053218'
            }
        }  
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'faktopedia-pl',
                    adUnitElementId: 'sky_left_inner',
                    placement: 'sky_left_inner',
                    environment: 'desktop',
                    }
                }               
    ]
    }
    ,        
    {
        code: 'sky_right_inner',
        mediaTypes: {
            banner: {
                sizes: [[300, 600]],
            }
        }
        ,
        bids: [ {
            bidder: 'adform',
            params: {
                mid: '348247'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId: 150872, pageId: 780966, formatId: 53517,
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2581, siteId: 461666, pageId: 1451618, formatId: 55370,
            }
        }
        ,
        {
            bidder: 'sspBC'
        } 
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '386578',
                    zoneId:'2153592',
                    sizes:'10',
            }
        } 
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1418176,
                publisherSubId:'sky_right_inner'
            }
        }  
        ,
    
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 22783799
            }
        }
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1053218'
            }
        }
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'faktopedia-pl',
                    adUnitElementId: 'sky_right_inner',
                    placement: 'sky_right_inner',
                    environment: 'desktop',
                    }
                } 
    
    ]
    }
    ,
    {
        code: 'top_rect_inner',
        mediaTypes: {
            banner: {
                sizes: [[336, 280]],
            }
        }
        ,
        bids: [ {
            bidder: 'adform',
            params: {
                mid: '180024'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId:  150872, pageId: 780349, formatId: 52161,
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2581, siteId:  461666, pageId: 1451622, formatId: 54226,
            }
        }
        ,
        {
            bidder: 'sspBC'
        } 
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '386578',
                    zoneId:'2153594',
                    sizes:'15',
            }
        }
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1418180,
                publisherSubId:'top_rect_inner'
            }
        }  
        ,
    
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        } 
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 22783799
            }
        }
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1053218'
            }
        }
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'faktopedia-pl',
                    adUnitElementId: 'top_rect_inner',
                    placement: 'top_rect_inner',
                    environment: 'desktop',
                    }
                } 
             
    
    ]
}
,
{
    code: 'middle_rect_inner',
    mediaTypes: {
        banner: {
            sizes: [[336, 280]],
        }
    }
    ,
    bids: [ {
        bidder: 'adform',
        params: {
            mid: '1142435'
        }
    }
    ,
    {
        bidder: 'smartadserver',
        params: {
            networkId: 2422, siteId:  150872, pageId: 780350, formatId: 52161,
        }
    }
    ,
    {
        bidder: 'smartadserver',
        params: {
            networkId: 2581, siteId:  461666, pageId: 1451621, formatId: 54306,
        }
    }
    ,
    {
        bidder: 'sspBC'
    } 
    ,
    {
        bidder: 'rubicon',
            params: {
                accountId: '21594',
                siteId: '386578',
                zoneId:'2153596',
                sizes:'15',
        }
    }  
    ,
    {
        bidder: 'criteo',
        params: {
            zoneId: 1418175,
            publisherSubId:'middle_rect_inner'
        }
    }   
    ,
    
    {
        bidder: "rtbhouse",
        params: {
            region: 'prebid-eu',
            publisherId: 'ubZbp6DokIAsAJBBqd3T'
        }
    }
    ,
    {
        bidder: 'oftmedia',
        params: {
            placementId: 22783799
        }
    }
    ,
    {
        bidder: 'connectad',
        params: {
           networkId: '10047',
           siteId: '1053218'
        }
    }
    ,
    {
        bidder: 'adagio',
        params:{
                organizationId: '1120',
                site: 'faktopedia-pl',
                adUnitElementId: 'middle_rect_inner',
                placement: 'middle_rect_inner',
                environment: 'desktop',
                }
            } 
              

]
}
    
];


var pbjs=pbjs || {};
pbjs.que=pbjs.que || [];
var USD=3.80;
var EUR=4.50;
pbjs.bidderSettings= {
    criteo: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*EUR;
        }
    }
    ,
    adform: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.9)*EUR;
        }
    }
    ,
    smartadserver: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm;
        }
    }
    ,
    rtbhouse: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    pulsepoint: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    emx_digital: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    onedisplay: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.85)*EUR;
        }
    }
    ,
    oftmedia: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.85)*USD;
        }
    }
    ,
    imonomy: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    rubicon: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.75)*USD;
        }
    }
    ,
    ix: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    connectad: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    amx: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    standard: {
        adserverTargeting: [ {
            key: "hb_bidder",
            val: function (bidResponse) {
                return bidResponse.bidderCode;
            }
        }
        ,
        {
            key: "hb_adid",
            val: function (bidResponse) {
                return bidResponse.adId;
            }
        }
        ,
        {
            key: "hb_pb",
            val: function(bidResponse) {
                var cpm=bidResponse.cpm;
                if (cpm < 10.00) {
                    return (Math.floor(cpm * 100) / 100).toFixed(2);
                }
                else {
                    return '10.00';
                }
            }
        }
        ]
    }
};