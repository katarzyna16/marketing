var googletag = null;
var pbjs = null;

const GPTScript = document.createElement('script');      
GPTScript.async = true;      
GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');      
top.document.head.appendChild(GPTScript);      

const AAScript = document.createElement('script');      
AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');      
AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');      
top.document.head.appendChild(AAScript);        

//#region initAdserver
function initAdserver() {
    if (pbjs.initAdserverSet) return;
    pbjs.initAdserverSet = true;
    googletag.cmd.push(function() {
        pbjs.que.push(function() {
            pbjs.setTargetingForGPTAsync();
            googletag.pubads().refresh();
        });
    });
}
//#endregion

setTimeout(() => {
    var div = document.createElement('div');
    div.style.textAlign = 'center';
    div.style.color = 'grey'
    div.style.fontSize = '10px'
    div.innerHTML = 'Reklama'
    var divClone = div.cloneNode(true);

    if (document.getElementById('rec_1') != null){
    document.getElementById('rec_1').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if (document.getElementById('rec_2') != null){
    document.getElementById('rec_2').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if (document.getElementById('rec_3') != null){
    document.getElementById('rec_3').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if (document.getElementById('rec_4') != null){
    document.getElementById('rec_4').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[0]) != null){
    document.getElementsByClassName('google-auto-placed')[0].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[1]) != null){
    document.getElementsByClassName('google-auto-placed')[1].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[2]) != null){
    document.getElementsByClassName('google-auto-placed')[2].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[3]) != null){
    document.getElementsByClassName('google-auto-placed')[3].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[4]) != null){
    document.getElementsByClassName('google-auto-placed')[4].prepend(divClone);
    }else{void(0);}
    
}, 5000);


window.googletag = window.googletag || {
    cmd: []
};
googletag.cmd.push(function() {
slot1 =    googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_mobile_1', [
            [336, 280],
            [300, 250]
        ], 'rec_1') //rec_1
        .addService(googletag.pubads());
slot2 =    googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_mobile_2', [
            [336, 280],
            [300, 250]
        ], 'rec_2') //rec_2
        .addService(googletag.pubads());
slot3 =    googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_mobile_3', [
            [336, 280],
            [300, 250],
            [320, 480],
            [320, 180]
        ], 'rec_3') //rec_3
        .addService(googletag.pubads());
slot4 =    googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_mobile_4', [
            [336, 280],
            [300, 250]
        ], 'rec_4') //rec_4
        .addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
    googletag.pubads().collapseEmptyDivs();
    googletag.pubads().setForceSafeFrame(false);
    googletag.pubads().setCentering(true);
});

googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
googletag.cmd.push(function() {
    googletag.pubads().disableInitialLoad();
});

pbjs = pbjs || {};
pbjs.que = pbjs.que || [];

pbjs.que.push(function() {
    pbjs.addAdUnits(adUnits);
    pbjs.setConfig({
        "schain":{ "validation": "strict", "config": { "ver":"1.0", "complete": 1, "nodes": [ { "asi":"yieldriser.com", "sid":"1", "hp":1 } ] }},
        bidderSequence: "random",
        consentManagement: {
            cmpApi: 'iab',
            timeout: 5000,
            allowAuctionWithoutConsent: true
        }
    });
    pbjs.requestBids({
        bidsBackHandler: initAdserver,
        timeout: PREBID_TIMEOUT
    });
});

//REC_1
let boxElement_rec_1;
let prevRatio_rec_1 = 0.0;
window.addEventListener("load", (event) => {
  boxElement_rec_1 = document.querySelector("#rec_1");
  createObserver_rec_1();
}, false);
function createObserver_rec_1() {
  let observer;
  let options = {
    root: null,
    rootMargin: "10%",
    threshold: 1.0
  };
  observer = new IntersectionObserver(handleIntersect_rec_1, options);
  observer = observer.observe(boxElement_rec_1);
}
var startTimeoutRefresh_rec_1;
var startIntervalRefresh_rec_1;
function startInterval_rec_1() {
    startTimeoutRefresh_rec_1 = setTimeout(refreshBid_rec_1, 5000);
    startIntervalRefresh_rec_1 = setInterval(refreshBid_rec_1, 15000);
    
  }
  
  function stopInterval_rec_1() {
    clearInterval(startIntervalRefresh_rec_1);
    clearTimeout(startTimeoutRefresh_rec_1);
    
  }
function handleIntersect_rec_1(entries, observer) {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > prevRatio_rec_1) {
      entry.target = startInterval_rec_1()
    } else {
      entry.target = stopInterval_rec_1()
    }
    prevRatio_rec_1 = entry.intersectionRatio;
  });
}

function refreshBid_rec_1() {
    console.log("refreshAdUnit")
    pbjs.que.push(function() {
        pbjs.requestBids({
            timeout: PREBID_TIMEOUT,
            adUnitCodes: ['rec_1'],
            bidsBackHandler: function() {
                pbjs.setTargetingForGPTAsync(['rec_1']);
                googletag.pubads().refresh([slot1]);
                googletag.cmd.push(function() {
                    googletag.display('rec_1');
                });
            }
        });
    });
}
//REC_2
let boxElement_rec_2;
let prevRatio_rec_2 = 0.0;
window.addEventListener("load", (event) => {
  boxElement_rec_2 = document.querySelector("#rec_2");
  createObserver_rec_2();
}, false);
function createObserver_rec_2() {
  let observer;
  let options = {
    root: null,
    rootMargin: "10%",
    threshold: 1.0
  };
  observer = new IntersectionObserver(handleIntersect_rec_2, options);
  observer = observer.observe(boxElement_rec_2);
}
var startTimeoutRefresh_rec_2;
var startIntervalRefresh_rec_2;
function startInterval_rec_2() {
    startTimeoutRefresh_rec_2 = setTimeout(refreshBid_rec_2, 5000);
    startIntervalRefresh_rec_2 = setInterval(refreshBid_rec_2, 30000);
    
  }
  
  function stopInterval_rec_2() {
    clearInterval(startIntervalRefresh_rec_2);
    clearTimeout(startTimeoutRefresh_rec_2);
    
  }
function handleIntersect_rec_2(entries, observer) {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > prevRatio_rec_2) {
      entry.target = startInterval_rec_2()
    } else {
      entry.target = stopInterval_rec_2()
    }
    prevRatio_rec_2 = entry.intersectionRatio;
  });
}

function refreshBid_rec_2() {
    console.log("refreshAdUnit")
    pbjs.que.push(function() {
        pbjs.requestBids({
            timeout: PREBID_TIMEOUT,
            adUnitCodes: ['rec_2'],
            bidsBackHandler: function() {
                pbjs.setTargetingForGPTAsync(['rec_2']);
                googletag.pubads().refresh([slot2]);
                googletag.cmd.push(function() {
                    googletag.display('rec_2');
                });
            }
        });
    });
}
//REC_3
let boxElement_rec_3;
let prevRatio_rec_3 = 0.0;
window.addEventListener("load", (event) => {
  boxElement_rec_3 = document.querySelector("#rec_3");
  createObserver_rec_3();
}, false);
function createObserver_rec_3() {
  let observer;
  let options = {
    root: null,
    rootMargin: "10%",
    threshold: 1.0
  };
  observer = new IntersectionObserver(handleIntersect_rec_3, options);
  observer = observer.observe(boxElement_rec_3);
}
var startTimeoutRefresh_rec_3;
var startIntervalRefresh_rec_3;
function startInterval_rec_3() {
    startTimeoutRefresh_rec_3 = setTimeout(refreshBid_rec_3, 5000);
    startIntervalRefresh_rec_3 = setInterval(refreshBid_rec_3, 30000);
    
  }
  
  function stopInterval_rec_3() {
    clearInterval(startIntervalRefresh_rec_3);
    clearTimeout(startTimeoutRefresh_rec_3);
    
  }
function handleIntersect_rec_3(entries, observer) {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > prevRatio_rec_3) {
      entry.target = startInterval_rec_3()
    } else {
      entry.target = stopInterval_rec_3()
    }
    prevRatio_rec_3 = entry.intersectionRatio;
  });
}

function refreshBid_rec_3() {
    console.log("refreshAdUnit")
    pbjs.que.push(function() {
        pbjs.requestBids({
            timeout: PREBID_TIMEOUT,
            adUnitCodes: ['rec_3'],
            bidsBackHandler: function() {
                pbjs.setTargetingForGPTAsync(['rec_3']);
                googletag.pubads().refresh([slot3]);
                googletag.cmd.push(function() {
                    googletag.display('rec_3');
                });
            }
        });
    });
}
//REC_4
let boxElement_rec_4;
let prevRatio_rec_4 = 0.0;
window.addEventListener("load", (event) => {
  boxElement_rec_4 = document.querySelector("#rec_4");
  createObserver_rec_4();
}, false);
function createObserver_rec_4() {
  let observer;
  let options = {
    root: null,
    rootMargin: "10%",
    threshold: 1.0
  };
  observer = new IntersectionObserver(handleIntersect_rec_4, options);
  observer = observer.observe(boxElement_rec_4);
}
var startTimeoutRefresh_rec_4;
var startIntervalRefresh_rec_4;
function startInterval_rec_4() {
    startTimeoutRefresh_rec_4 = setTimeout(refreshBid_rec_4, 5000);
    startIntervalRefresh_rec_4 = setInterval(refreshBid_rec_4, 30000);
    
  }
  
  function stopInterval_rec_4() {
    clearInterval(startIntervalRefresh_rec_4);
    clearTimeout(startTimeoutRefresh_rec_4);
    
  }
function handleIntersect_rec_4(entries, observer) {
  entries.forEach((entry) => {
    if (entry.intersectionRatio > prevRatio_rec_4) {
      entry.target = startInterval_rec_4()
    } else {
      entry.target = stopInterval_rec_4()
    }
    prevRatio_rec_4 = entry.intersectionRatio;
  });
}

function refreshBid_rec_4() {
    console.log("refreshAdUnit")
    pbjs.que.push(function() {
        pbjs.requestBids({
            timeout: PREBID_TIMEOUT,
            adUnitCodes: ['rec_4'],
            bidsBackHandler: function() {
                pbjs.setTargetingForGPTAsync(['rec_4']);
                googletag.pubads().refresh([slot4]);
                googletag.cmd.push(function() {
                    googletag.display('rec_4');
                });
            }
        });
    });
}

setInterval(checkIfDocIsHidden, 2000);
function checkIfDocIsHidden(){
if(document.hidden){    
    stopInterval_rec_1();
    stopInterval_rec_2();
    stopInterval_rec_3();
    stopInterval_rec_4();
}}

// setInterval(refreshBid, 30000);

//googletag.cmd.push(function() { googletag.display('rec_1'); }); //customad 2



var PREBID_TIMEOUT=1200;
var adUnits=[ //#region headerAD    
   {
        code: 'rec_1',
        mediaTypes: {
            banner: {
                sizes: [[336, 280], [300, 250]],
            }
        }
        ,
        bids: [ {
            bidder: 'adform',
            params: {
                mid: '944165'
            }
        }
        ,
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                domain: '//www14.smartadserver.com', siteId: 145384, pageId: 770176, formatId: 52161,
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                domain: '//prg.smartadserver.com', siteId: 162117, pageId: 996330, formatId: 54226,
            }
        }  
        ,
        {
            bidder: 'emx_digital',
            params: {
                tagid: '104354'
            }
        }  
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 21068619
            }
        }
        ,
        {
            bidder: 'criteo',
            params: {
                networkId: 7049
            }
        }
        ,
        {
            bidder: 'connectad',
            params: {
                networkId: '10047', siteId: '1033590'
            }
        }
        ,
        {
            bidder: 'ix',
            params: {
                siteId: '477688', size: [300, 250]
            }
        }
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '316854',
                    zoneId:'1624184',
                    sizes:'16',
                    }
        }
        ,
        {
            bidder: 'visx',
                 params: {
                           uid: '914976'
                       }
        }
        ,
        {
            bidder: "amx",
                params: {
                      memberId: "152media"
                    }
        }
        ,
        {
            bidder: 'selectmedia',
                params: {
                        aid: 606504
                    }
    } 
    ,
    {
        bidder: 'adagio',
        params:{
            organizationId: '1120',
            site: 'mistrzowie-org',
            adUnitElementId: 'rec_1',
            placement: 'rec_1',
            environment: 'mobile',
        }
    }   
    ,
    {
            bidder: 'sspBC'
            }       
        ]
    }
    
    ,
    {
        code: 'rec_2',
        mediaTypes: {
            banner: {
                sizes: [[336, 280], [300, 250]],
            }
        }
        ,
        bids: [ {
            bidder: 'adform',
            params: {
                mid: '944166'
            }
        }
        ,
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                domain: '//www14.smartadserver.com', siteId: 145384, pageId: 770176, formatId: 52161,
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                domain: '//prg.smartadserver.com', siteId: 162117, pageId: 996330, formatId: 54226,
            }
        }  
        ,
        {
            bidder: 'emx_digital',
            params: {
                tagid: '104354'
            }
        }      
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 21068619
            }
        }
        ,
        {
            bidder: 'criteo',
            params: {
                networkId: 7049
            }
        }
        ,
        {
            bidder: 'connectad',
            params: {
                networkId: '10047', siteId: '1033590'
            }
        }
        ,
        {
            bidder: 'ix',
            params: {
                siteId: '477688', size: [300, 250]
            }
        }
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '316854',
                    zoneId:'1624184',
                    sizes:'16',
                    }
        }   
        ,
        {
            bidder: 'visx',
                 params: {
                           uid: '914977'
                       }
        }
        ,
        {
            bidder: "amx",
                params: {
                      memberId: "152media"
                    }
        }
        ,
        {
            bidder: 'selectmedia',
                params: {
                    aid: 606504
                    }
    }
    ,
    {
        bidder: 'adagio',
        params:{
            organizationId: '1120',
            site: 'mistrzowie-org',
            adUnitElementId: 'rec_2',
            placement: 'rec_2',
            environment: 'mobile',
        }
    } 
    ,
    {
            bidder: 'sspBC'
            }     
        ]
    }
    
    ,
    {
        code: 'rec_3',
        mediaTypes: {
            banner: {
                sizes: [[336, 280], [300, 250], [320, 180], [320, 480]],
            }
        }
        ,
        bids: [ {
            bidder: 'adform',
            params: {
                mid: '944167'
            }
        }
        ,
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                domain: '//www14.smartadserver.com', siteId: 145384, pageId: 770177, formatId: 52161,
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                domain: '//prg.smartadserver.com', siteId: 162117, pageId: 996330, formatId: 54226,
            }
        }  
        ,
        {
            bidder: 'emx_digital',
            params: {
                tagid: '104354'
            }
        }      
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 21068619
            }
        }
        ,
        {
            bidder: 'criteo',
            params: {
                networkId: 7049
            }
        }
        ,
        {
            bidder: 'connectad',
            params: {
                networkId: '10047', siteId: '1033590'
            }
        }
        ,
        {
            bidder: 'ix',
            params: {
                siteId: '477688', size: [300, 250]
            }
        }
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '316854',
                    zoneId:'1624184',
                    sizes:'16',
                       }
        }
        ,
        {
            bidder: 'visx',
            params: {
                uid: '914978'
            }
        }
        ,
        {
            bidder: "amx",
            params: {
              memberId: "152media"
            }
        }
        ,
        {
            bidder: 'selectmedia',
                params: {
                    aid: 606504
                    }
        }
        ,
        {
            bidder: 'adagio',
            params:{
            organizationId: '1120',
            site: 'mistrzowie-org',
            adUnitElementId: 'rec_3',
            placement: 'rec_3',
            environment: 'mobile',
        }
        } 
        ,
        {
            bidder: 'sspBC'
        } 
        ]
    }
    
    ,
    {
        code: 'rec_4',
        mediaTypes: {
            banner: {
                sizes: [[336, 280], [300, 250]],
            }
        }
        ,
        bids: [ {
            bidder: 'adform',
            params: {
                mid: '944168'
            }
        }
        ,
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu', publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                domain: '//www14.smartadserver.com', siteId: 145384, pageId: 770177, formatId: 52161,
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                domain: '//prg.smartadserver.com', siteId: 162117, pageId: 996330, formatId: 54226,
            }
        }  
        ,
        {
            bidder: 'emx_digital',
            params: {
                tagid: '104354'
            }
        }      
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 21068619
            }
        }
        ,
        {
            bidder: 'criteo',
            params: {
                networkId: 7049
            }
        }
        ,
        {
            bidder: 'connectad',
            params: {
                networkId: '10047', siteId: '1033590'
            }
        }
        ,
        {
            bidder: 'ix',
            params: {
                siteId: '477688', size: [300, 250]
            }
        }
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '316854',
                    zoneId:'1624184',
                    sizes:'16',
                    }
        }
        ,
        {
            bidder: 'visx',
                params: {
                           uid: '914979'
                       }
        }
        ,
        {
            bidder: "amx",
                params: {
                      memberId: "152media"
                    }
        }
        ,
        {
            bidder: 'selectmedia',
                params: {
                aid: 606504
        }
        }   
        ,
        {
            bidder: 'adagio',
            params:{
            organizationId: '1120',
            site: 'mistrzowie-org',
            adUnitElementId: 'rec_4',
            placement: 'rec_4',
            environment: 'mobile',
            }
        } 
        ,
        {
            bidder: 'sspBC'
        }         
        ]
    }    
    ];
//#endregion
//#region waluty, przeliczenia, buckety
var pbjs=pbjs || {};
pbjs.que=pbjs.que || [];
var USD=3.80;
var EUR=4.50;
pbjs.bidderSettings= {
    criteo: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*EUR;
        }
    }
    ,
    adform: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.9)*EUR;
        }
    }
    ,
    smartadserver: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm;
        }
    }
    ,
    rtbhouse: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    pulsepoint: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    emx_digital: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    onedisplay: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.85)*EUR;
        }
    }
    ,
    oftmedia: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.85)*USD;
        }
    }
    ,
    imonomy: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    rubicon: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.75)*USD;
        }
    }
    ,
    ix: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    connectad: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    amx: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm;
        }
    }
    ,
    visx: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm;
        }
    }
    ,
    sspBC: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm;
        }
    }
    ,
    standard: {
        adserverTargeting: [ {
            key: "hb_bidder",
            val: function (bidResponse) {
                return bidResponse.bidderCode;
            }
        }
        ,
        {
            key: "hb_adid",
            val: function (bidResponse) {
                return bidResponse.adId;
            }
        }
        ,
        {
            key: "hb_pb",
            val: function(bidResponse) {
                var cpm=bidResponse.cpm;
                if (cpm < 10.00) {
                    return (Math.floor(cpm * 100) / 100).toFixed(2);
                }
                else {
                    return '10.00';
                }
            }
        }
        ]
    }
};