    var googletag = null;
    var pbjs = null;
    
    var PREBID_TIMEOUT=3000;
    
    const GPTScript = document.createElement('script');      
    GPTScript.async = true;      
    GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');      
    top.document.head.appendChild(GPTScript);      
    
    const AAScript = document.createElement('script');      
    AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');      
    AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');      
    top.document.head.appendChild(AAScript);   
    
    //#region initAdserver
    function initAdserver() {
        if (pbjs.initAdserverSet) return;
        pbjs.initAdserverSet = true;
        googletag.cmd.push(function() {
            pbjs.que.push(function() {
                pbjs.setTargetingForGPTAsync();
                googletag.pubads().refresh();
            });
        });
    }
    //#endregion
    
    
    setTimeout(() => {
        var div = document.createElement('div');
        div.style.textAlign = 'center';
        div.style.color = 'grey'
        div.style.fontSize = '10px'
        div.innerHTML = 'Reklama'
        var divClone = div.cloneNode(true);
        if (document.getElementById('main_billboard') != null){
        document.getElementById('main_billboard').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('rightRect') != null){
        document.getElementById('rightRect').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('leftSky') != null){
        document.getElementById('leftSky').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('rightSky') != null){
        document.getElementById('rightSky').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('middleRect') != null){
        document.getElementById('middleRect').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if (document.getElementById('bottomRect') != null){
        document.getElementById('bottomRect').prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if ((document.getElementsByClassName('google-auto-placed')[0]) != null){
        document.getElementsByClassName('google-auto-placed')[0].prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if ((document.getElementsByClassName('google-auto-placed')[1]) != null){
        document.getElementsByClassName('google-auto-placed')[1].prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if ((document.getElementsByClassName('google-auto-placed')[2]) != null){
        document.getElementsByClassName('google-auto-placed')[2].prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if ((document.getElementsByClassName('google-auto-placed')[3]) != null){
        document.getElementsByClassName('google-auto-placed')[3].prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if ((document.getElementsByClassName('google-auto-placed')[4]) != null){
        document.getElementsByClassName('google-auto-placed')[4].prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if ((document.getElementsByClassName('google-auto-placed')[5]) != null){
        document.getElementsByClassName('google-auto-placed')[5].prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
        if ((document.getElementsByClassName('google-auto-placed')[6]) != null){
        document.getElementsByClassName('google-auto-placed')[6].prepend(divClone);
        divClone = div.cloneNode(true);
        }else{void(0);}
      
    
    }, 5000);
    
    window.googletag = window.googletag || {
        cmd: []
    };
    
    
    
    
    var slot1, slot2, slot3, slot4, slot5, slot6, slot7;
    googletag.cmd.push(function() {   
    
    slot1 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_top_bill', [[750, 200]], 'top_bill_inner') //top_bill_inner
            .addService(googletag.pubads());
    slot2 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_left_Sky', [[160, 600]], 'left_Sky_inner') //left_Sky_inner
            .addService(googletag.pubads());
    slot3 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_right_Sky', [[300, 600]], 'right_Sky_inner') //right_Sky_inner
            .addService(googletag.pubads());
    slot4 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_right_rect', [[300, 250]], 'right_rect_inner') //right_rect_inner
            .addService(googletag.pubads());
    slot5 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_middle_rect', [[300, 250],[336, 280]], 'middle_rect_inner') //middle_rect_inner
            .addService(googletag.pubads());
    slot6 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_bottom_rect', [[300, 250],[336, 280]], 'bottom_rect_inner') //bottom_rect_inner
            .addService(googletag.pubads());  
         
            
        googletag.pubads().enableSingleRequest();
        googletag.pubads().disableInitialLoad();    
        googletag.pubads().collapseEmptyDivs();
        googletag.pubads().setForceSafeFrame(false);
        googletag.pubads().setCentering(true);
        googletag.enableServices();
    });
    
    googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    
    pbjs = pbjs || {};
    pbjs.que = pbjs.que || [];
    
    pbjs.que.push(function() {
        pbjs.addAdUnits(adUnits);
        pbjs.setConfig({
            "schain":{ "validation": "strict", "config": { "ver":"1.0", "complete": 1, "nodes": [ { "asi":"yieldriser.com", "sid":"26", "hp":1 } ] }},
            bidderSequence: "random",
            disableAjaxTimeout: true,
            consentManagement: {
                cmpApi: 'iab',
                timeout: 5000,
                allowAuctionWithoutConsent: true
            }
        });
        pbjs.requestBids({
            bidsBackHandler: initAdserver,
            timeout: PREBID_TIMEOUT
        });
    });
    
    //top_bill_inner
    let boxElement_top_bill_inner;
    let prevRatio_top_bill_inner = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('top_bill_inner') != null){
      boxElement_top_bill_inner = document.getElementById("top_bill_inner");
      createObserver_top_bill_inner();
    }}, false);
    function createObserver_top_bill_inner() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_top_bill_inner, options);
      observer = observer.observe(boxElement_top_bill_inner);
    }
    var startTimeoutRefresh_top_bill_inner;
    var startIntervalRefresh_top_bill_inner;
    function startInterval_top_bill_inner() {
        startIntervalRefresh_top_bill_inner = setInterval(refreshBid_top_bill_inner, 10000);
        
      }
      
      function stopInterval_top_bill_inner() {
        clearInterval(startIntervalRefresh_top_bill_inner);
        
      }
    function handleIntersect_top_bill_inner(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_top_bill_inner && !document.hidden) {
          entry.target = startInterval_top_bill_inner()
        } else {
          entry.target = stopInterval_top_bill_inner()
        }
        prevRatio_top_bill_inner = entry.intersectionRatio;
      });
    }
    
    function refreshBid_top_bill_inner() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['top_bill_inner'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['top_bill_inner']);
                    googletag.pubads().refresh([slot1]);
                    googletag.cmd.push(function() {
                        googletag.display('top_bill_inner');
                    });
                }
            });
        });
    }
    
    
    //left_Sky_inner
    let boxElement_left_Sky_inner;
    let prevRatio_left_Sky_inner = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('left_Sky_inner') != null){
      boxElement_left_Sky_inner = document.getElementById("left_Sky_inner");
      createObserver_left_Sky_inner();
    }}, false);
    function createObserver_left_Sky_inner() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_left_Sky_inner, options);
      observer = observer.observe(boxElement_left_Sky_inner);
    }
    var startTimeoutRefresh_left_Sky_inner;
    var startIntervalRefresh_left_Sky_inner;
    function startInterval_left_Sky_inner() {
        startIntervalRefresh_left_Sky_inner = setInterval(refreshBid_left_Sky_inner, 10000);
        
      }
      
      function stopInterval_left_Sky_inner() {
        clearInterval(startIntervalRefresh_left_Sky_inner);
        
      }
    function handleIntersect_left_Sky_inner(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_left_Sky_inner && !document.hidden) {
          entry.target = startInterval_left_Sky_inner()
        } else {
          entry.target = stopInterval_left_Sky_inner()
        }
        prevRatio_left_Sky_inner = entry.intersectionRatio;
      });
    }
    
    function refreshBid_left_Sky_inner() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['left_Sky_inner'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['left_Sky_inner']);
                    googletag.pubads().refresh([slot2]);
                    googletag.cmd.push(function() {
                        googletag.display('left_Sky_inner');
                    });
                }
            });
        });
    }
    
    //right_Sky_inner
    let boxElement_right_Sky_inner;
    let prevRatio_right_Sky_inner = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('right_Sky_inner') != null){
      boxElement_right_Sky_inner = document.getElementById("right_Sky_inner");
      createObserver_right_Sky_inner();
    }}, false);
    function createObserver_right_Sky_inner() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_right_Sky_inner, options);
      observer = observer.observe(boxElement_right_Sky_inner);
    }
    var startTimeoutRefresh_right_Sky_inner;
    var startIntervalRefresh_right_Sky_inner;
    function startInterval_right_Sky_inner() {
        startIntervalRefresh_right_Sky_inner = setInterval(refreshBid_right_Sky_inner, 10000);
        
      }
      
      function stopInterval_right_Sky_inner() {
        clearInterval(startIntervalRefresh_right_Sky_inner);
        
      }
    function handleIntersect_right_Sky_inner(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_right_Sky_inner && !document.hidden) {
          entry.target = startInterval_right_Sky_inner()
        } else {
          entry.target = stopInterval_right_Sky_inner()
        }
        prevRatio_right_Sky_inner = entry.intersectionRatio;
      });
    }
    
    function refreshBid_right_Sky_inner() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['right_Sky_inner'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['right_Sky_inner']);
                    googletag.pubads().refresh([slot3]);
                    googletag.cmd.push(function() {
                        googletag.display('right_Sky_inner');
                    });
                }
            });
        });
    }
    
    //right_rect_inner
    let boxElement_right_rect_inner;
    let prevRatio_right_rect_inner = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('right_rect_inner') != null){
      boxElement_right_rect_inner = document.getElementById("right_rect_inner");
      createObserver_right_rect_inner();
    }}, false);
    function createObserver_right_rect_inner() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_right_rect_inner, options);
      observer = observer.observe(boxElement_right_rect_inner);
    }
    var startTimeoutRefresh_right_rect_inner;
    var startIntervalRefresh_right_rect_inner;
    function startInterval_right_rect_inner() {
        startIntervalRefresh_right_rect_inner = setInterval(refreshBid_right_rect_inner, 10000);
       
      }
      
      function stopInterval_right_rect_inner() {
        clearInterval(startIntervalRefresh_right_rect_inner);
       
      }
    function handleIntersect_right_rect_inner(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_right_rect_inner && !document.hidden) {
          entry.target = startInterval_right_rect_inner()
        } else {
          entry.target = stopInterval_right_rect_inner()
        }
        prevRatio_right_rect_inner = entry.intersectionRatio;
      });
    }
    
    function refreshBid_right_rect_inner() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['right_rect_inner'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['right_rect_inner']);
                    googletag.pubads().refresh([slot4]);
                    googletag.cmd.push(function() {
                        googletag.display('right_rect_inner');
                    });
                }
            });
        });
    }
    
    //middle_rect_inner
    let boxElement_middle_rect_inner;
    let prevRatio_middle_rect_inner = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('middle_rect_inner') != null){
      boxElement_middle_rect_inner = document.getElementById("middle_rect_inner");
      createObserver_middle_rect_inner();
    }}, false);
    function createObserver_middle_rect_inner() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_middle_rect_inner, options);
      observer = observer.observe(boxElement_middle_rect_inner);
    }
    var startTimeoutRefresh_middle_rect_inner;
    var startIntervalRefresh_middle_rect_inner;
    function startInterval_middle_rect_inner() {
        startIntervalRefresh_middle_rect_inner = setInterval(refreshBid_middle_rect_inner, 10000);
        
      }
      
      function stopInterval_middle_rect_inner() {
        clearInterval(startIntervalRefresh_middle_rect_inner);
       
      }
    function handleIntersect_middle_rect_inner(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_middle_rect_inner && !document.hidden) {
          entry.target = startInterval_middle_rect_inner()
        } else {
          entry.target = stopInterval_middle_rect_inner()
        }
        prevRatio_middle_rect_inner = entry.intersectionRatio;
      });
    }
    
    function refreshBid_middle_rect_inner() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['middle_rect_inner'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['middle_rect_inner']);
                    googletag.pubads().refresh([slot5]);
                    googletag.cmd.push(function() {
                        googletag.display('middle_rect_inner');
                    });
                }
            });
        });
    }
    //bottom_rect_inner
    let boxElement_bottom_rect_inner;
    let prevRatio_bottom_rect_inner = 0.0;
    window.addEventListener("load", (event) => {
        if (document.getElementById('bottom_rect_inner') != null){
      boxElement_bottom_rect_inner = document.getElementById("bottom_rect_inner");
      createObserver_bottom_rect_inner();
    }}, false);
    function createObserver_bottom_rect_inner() {
      let observer;
      let options = {
        root: null,
        rootMargin: "10%",
        threshold: 1.0
      };
      observer = new IntersectionObserver(handleIntersect_bottom_rect_inner, options);
      observer = observer.observe(boxElement_bottom_rect_inner);
    }
    var startTimeoutRefresh_bottom_rect_inner;
    var startIntervalRefresh_bottom_rect_inner;
    function startInterval_bottom_rect_inner() {
        startIntervalRefresh_bottom_rect_inner = setInterval(refreshBid_bottom_rect_inner, 10000);
        
      }
      
      function stopInterval_bottom_rect_inner() {
        clearInterval(startIntervalRefresh_bottom_rect_inner);
       
      }
    function handleIntersect_bottom_rect_inner(entries, observer) {
      entries.forEach((entry) => {
        if (entry.intersectionRatio > prevRatio_bottom_rect_inner && !document.hidden) {
          entry.target = startInterval_bottom_rect_inner()
        } else {
          entry.target = stopInterval_bottom_rect_inner()
        }
        prevRatio_bottom_rect_inner = entry.intersectionRatio;
      });
    }
    
    function refreshBid_bottom_rect_inner() {
        console.log("refreshAdUnit")
        pbjs.que.push(function() {
            pbjs.requestBids({
                timeout: PREBID_TIMEOUT,
                adUnitCodes: ['bottom_rect_inner'],
                bidsBackHandler: function() {
                    pbjs.setTargetingForGPTAsync(['bottom_rect_inner']);
                    googletag.pubads().refresh([slot5]);
                    googletag.cmd.push(function() {
                        googletag.display('bottom_rect_inner');
                    });
                }
            });
        });
    }
    
    
    
    
    
    setInterval(checkIfDocIsHidden, 2000);
    function checkIfDocIsHidden(){
    if(document.hidden){    
        stopInterval_top_bill_inner();
        stopInterval_left_Sky_inner();
        stopInterval_right_Sky_inner();
        stopInterval_right_rect_inner();
        stopInterval_middle_rect_inner();
        stopInterval_bottom_rect_inner();
       
    
    }}
    
    //biders
    
    var adUnits=[
        {
            code: 'top_bill_inner',
            mediaTypes: {
                banner: {
                    sizes: [[750, 200]],
                }
            }
            ,
            bids: [ 
            {
                bidder: 'adform',
                params: {
                    mid: '90242'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId: 154914, pageId: 930696, formatId: 51751,
                }
            }
            // ,
            // {
            //     bidder: 'smartadserver',
            //     params: {
            //         networkId: 2581, siteId: 461666, pageId: 1451620, formatId: 56391,
            //     }
            // }
            ,
            {
                bidder: 'sspBC'
            }  
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '396252',
                        zoneId:'2212972',
                        sizes:'40',
                }
            }   
            ,
            {
                bidder: 'criteo',
                params: {
                    zoneId: 1418179,
                    publisherSubId:'top_bill_inner'
                }
            } 
            ,
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu',
                    publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }  
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 23318777
                }
            }  
            ,
            {
                bidder: 'connectad',
                params: {
                   networkId: '10047',
                   siteId: '1054371'
                }
            }   
            ,
            {
                bidder: 'adagio',
                params:{
                        organizationId: '1120',
                        site: 'joemonster-org',
                        adUnitElementId: 'top_bill_inner',
                        placement: 'top_bill_inner',
                        environment: 'desktop',
                        }
                    }
        ]
        }
    ,
    {
        code: 'left_Sky_inner',
        mediaTypes: {
            banner: {
                sizes: [[160, 600]],
            }
        }
        ,
        bids: [ 
        {
            bidder: 'adform',
            params: {
                mid: '139548'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId: 154914, pageId: 930695, formatId: 53517,
            }
        }
        // ,
        // {
        //     bidder: 'smartadserver',
        //     params: {
        //         networkId: 2581, siteId: 461666, pageId: 1451620, formatId: 56391,
        //     }
        // }
        ,
        {
            bidder: 'sspBC'
        }  
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '396252',
                    zoneId:'2212964',
                    sizes:'9',
            }
        }   
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1421652,
                publisherSubId:'left_Sky_inner'
            }
        } 
        ,
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }  
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 23318777
            }
        }  
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1054371'
            }
        }   
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'joemonster-org',
                    adUnitElementId: 'left_Sky_inner',
                    placement: 'left_Sky_inner',
                    environment: 'desktop',
                    }
                }
    ]
    }  
    ,
    {
        code: 'right_rect_inner',
        mediaTypes: {
            banner: {
                sizes: [[300, 250]],
            }
        }
        ,
        bids: [ 
        {
            bidder: 'adform',
            params: {
                mid: '414599'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId: 154914, pageId: 930697, formatId: 52161,
            }
        }
        // ,
        // {
        //     bidder: 'smartadserver',
        //     params: {
        //         networkId: 2581, siteId: 461666, pageId: 1451620, formatId: 56391,
        //     }
        // }
        ,
        {
            bidder: 'sspBC'
        }  
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '396252',
                    zoneId:'2212970',
                    sizes:'16',
            }
        }
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1418180,
                publisherSubId:'right_rect_inner'
            }
        } 
        ,
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }  
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 23318777
            }
        }  
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1054371'
            }
        }   
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'joemonster-org',
                    adUnitElementId: 'right_rect_inner',
                    placement: 'right_rect_inner',
                    environment: 'desktop',
                    }
                }
    ]
    }   
    ,
    {
        code: 'right_Sky_inner',
        mediaTypes: {
            banner: {
                sizes: [[300, 600]],
            }
        }
        ,
        bids: [ 
        {
            bidder: 'adform',
            params: {
                mid: '91830'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId: 154914, pageId: 930693, formatId: 65479,
            }
        }
        // ,
        // {
        //     bidder: 'smartadserver',
        //     params: {
        //         networkId: 2581, siteId: 461666, pageId: 1451620, formatId: 56391,
        //     }
        // }
        ,
        {
            bidder: 'sspBC'
        }  
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '396252',
                    zoneId:'2212966',
                    sizes:'10',
            }
        }   
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1418176,
                publisherSubId:'right_Sky_inner'
            }
        } 
        ,
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }  
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 23318777
            }
        }  
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1054371'
            }
        }   
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'joemonster-org',
                    adUnitElementId: 'right_Sky_inner',
                    placement: 'right_Sky_inner',
                    environment: 'desktop',
                    }
                }
    ]
    }   
    ,
    {
        code: 'middle_rect_inner',
        mediaTypes: {
            banner: {
                sizes: [[336, 280]],
            }
        }
        ,
        bids: [ 
        {
            bidder: 'adform',
            params: {
                mid: '414563'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId: 154914, pageId: 949353, formatId: 65872,
            }
        }
        // ,
        // {
        //     bidder: 'smartadserver',
        //     params: {
        //         networkId: 2581, siteId: 461666, pageId: 1451620, formatId: 56391,
        //     }
        // }
        ,
        {
            bidder: 'sspBC'
        }  
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '396252',
                    zoneId:'2212970',
                    sizes:'16',
            }
        } 
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1418180,
                publisherSubId:'middle_rect_inner'
            }
        } 
        ,
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }  
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 23318777
            }
        }  
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1054371'
            }
        }   
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'joemonster-org',
                    adUnitElementId: 'middle_rect_inner',
                    placement: 'middle_rect_inner',
                    environment: 'desktop',
                    }
                }
    ]
    }   
    ,
    {
        code: 'bottom_rect_inner',
        mediaTypes: {
            banner: {
                sizes: [[336, 280]],
            }
        }
        ,
        bids: [ 
        {
            bidder: 'adform',
            params: {
                mid: '655345'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId: 154914, pageId: 949354, formatId: 76184,
            }
        }
        // ,
        // {
        //     bidder: 'smartadserver',
        //     params: {
        //         networkId: 2581, siteId: 461666, pageId: 1451620, formatId: 56391,
        //     }
        // }
        ,
        {
            bidder: 'sspBC'
        }  
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '396252',
                    zoneId:'2212970',
                    sizes:'16',
            }
        }  
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1418180,
                publisherSubId:'bottom_rect_inner'
            }
        } 
        ,
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        }  
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 23318777
            }
        }  
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1054371'
            }
        }   
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'joemonster-org',
                    adUnitElementId: 'bottom_rect_inner',
                    placement: 'bottom_rect_inner',
                    environment: 'desktop',
                    }
                }
    ]
    } 
    // ,
    // {
    //     code: 'rec_3_inner',
    //     mediaTypes: {
    //         banner: {
    //             sizes: [[336, 280]],
    //         }
    //     }
    //     ,
    //     bids: [ 
    //     {
    //         bidder: 'adform',
    //         params: {
    //             mid: '414575'
    //         }
    //     }
    //     ,
    //     {
    //         bidder: 'smartadserver',
    //         params: {
    //             networkId: 2422, siteId: 154914, pageId: 821733, formatId: 107434,
    //         }
    //     }
    //     ,
    //     {
    //         bidder: 'smartadserver',
    //         params: {
    //             networkId: 2581, siteId: 461666, pageId: 1451620, formatId: 56391,
    //         }
    //     }
    //     ,
    //     {
    //         bidder: 'sspBC'
    //     }  
    //     ,
    //     {
    //         bidder: 'rubicon',
    //             params: {
    //                 accountId: '21594',
    //                 siteId: '396252',
    //                 zoneId:'2212970',
    //                 sizes:'16',
    //         }
    //     }  
    //     ,
    //     {
    //         bidder: 'criteo',
    //         params: {
    //             zoneId: 1418180,
    //             publisherSubId:'rec_3_inner'
    //         }
    //     } 
    //     ,
    //     {
    //         bidder: "rtbhouse",
    //         params: {
    //             region: 'prebid-eu',
    //             publisherId: 'ubZbp6DokIAsAJBBqd3T'
    //         }
    //     }  
    //     ,
    //     {
    //         bidder: 'oftmedia',
    //         params: {
    //             placementId: 23318777
    //         }
    //     }  
    //     ,
    //     {
    //         bidder: 'connectad',
    //         params: {
    //            networkId: '10047',
    //            siteId: '1054371'
    //         }
    //     }   
    //     ,
    //     {
    //         bidder: 'adagio',
    //         params:{
    //                 organizationId: '1120',
    //                 site: 'joemonster-org',
    //                 adUnitElementId: 'rec_3_inner',
    //                 placement: 'rec_3_inner',
    //                 environment: 'desktop',
    //                 }
    //             }
    // ]
    // }    
    ];
    
    
    var pbjs=pbjs || {};
    pbjs.que=pbjs.que || [];
    var USD=3.80;
    var EUR=4.50;
    pbjs.bidderSettings= {
        criteo: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*EUR;
            }
        }
        ,
        adform: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.9)*EUR;
            }
        }
        ,
        smartadserver: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm;
            }
        }
        ,
        rtbhouse: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        pulsepoint: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        emx_digital: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        onedisplay: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.85)*EUR;
            }
        }
        ,
        oftmedia: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.85)*USD;
            }
        }
        ,
        imonomy: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        rubicon: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.75)*USD;
            }
        }
        ,
        ix: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        connectad: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        amx: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        standard: {
            adserverTargeting: [ {
                key: "hb_bidder",
                val: function (bidResponse) {
                    return bidResponse.bidderCode;
                }
            }
            ,
            {
                key: "hb_adid",
                val: function (bidResponse) {
                    return bidResponse.adId;
                }
            }
            ,
            {
                key: "hb_pb",
                val: function(bidResponse) {
                    var cpm=bidResponse.cpm;
                    if (cpm < 10.00) {
                        return (Math.floor(cpm * 100) / 100).toFixed(2);
                    }
                    else {
                        return '10.00';
                    }
                }
            }
            ]
        }
    };
    