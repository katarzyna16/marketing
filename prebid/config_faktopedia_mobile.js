var googletag = null;
var pbjs = null;

const GPTScript = document.createElement('script');      
GPTScript.async = true;      
GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');      
top.document.head.appendChild(GPTScript);      

const AAScript = document.createElement('script');      
AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');      
AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');      
top.document.head.appendChild(AAScript);        

//#region initAdserver
function initAdserver() {
    if (pbjs.initAdserverSet) return;
    pbjs.initAdserverSet = true;
    googletag.cmd.push(function() {
        pbjs.que.push(function() {
            pbjs.setTargetingForGPTAsync();
            googletag.pubads().refresh();
        });
    });
}
//#endregion

//div "reklama"
setTimeout(() => {
    var div = document.createElement('div');
    div.style.textAlign = 'center';
    div.style.color = 'grey'
    div.style.fontSize = '10px'
    div.innerHTML = 'Reklama'
    var divClone = div.cloneNode(true);

    if (document.getElementById('top_bill') != null){
    document.getElementById('top_bill').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if (document.getElementById('middle_rect') != null){
    document.getElementById('middle_rect').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if (document.getElementById('bottom_rect') != null){
    document.getElementById('bottom_rect').prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[0]) != null){
    document.getElementsByClassName('google-auto-placed')[0].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[1]) != null){
    document.getElementsByClassName('google-auto-placed')[1].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[2]) != null){
    document.getElementsByClassName('google-auto-placed')[2].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[3]) != null){
    document.getElementsByClassName('google-auto-placed')[3].prepend(divClone);
    divClone = div.cloneNode(true);
    }else{void(0);}
    if ((document.getElementsByClassName('google-auto-placed')[4]) != null){
    document.getElementsByClassName('google-auto-placed')[4].prepend(divClone);
    }else{void(0);}
    
}, 5000);
// end region

window.googletag = window.googletag || {
    cmd: []
};

googletag.cmd.push(function() {
    googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/2870248169', [[336, 280],[300, 250]], 'top_bill') //top_bill
        .addService(googletag.pubads());
    googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/faktopedia.pl_300x250_mobile_middle_rect', [[336, 280],[300, 250]], 'middle_rect') //middle_rect
        .addService(googletag.pubads());
    googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/faktopedia.pl_300x250_mobile_bottom_rect', [[336, 280],[300, 250]], 'bottom_rect') //bottom_rect
        .addService(googletag.pubads());

    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
    googletag.pubads().collapseEmptyDivs();
    googletag.pubads().setForceSafeFrame(false);
    googletag.pubads().setCentering(true);
});


//???
googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
googletag.cmd.push(function() {
    googletag.pubads().disableInitialLoad();
}); //???


//schain / consent
pbjs = pbjs || {};
pbjs.que = pbjs.que || [];

pbjs.que.push(function() {
    pbjs.addAdUnits(adUnits);
    pbjs.setConfig({
        "schain":{ "validation": "strict", "config": { "ver":"1.0", "complete": 1, "nodes": [ { "asi":"yieldriser.com", "sid":"28", "hp":1 } ] }},
        bidderSequence: "random",
        consentManagement: {
            cmpApi: 'iab',
            timeout: 5000,
            allowAuctionWithoutConsent: true
        }
    });
    pbjs.requestBids({
        bidsBackHandler: initAdserver,
        timeout: PREBID_TIMEOUT
    });
});



var PREBID_TIMEOUT=1200;
var adUnits=[ //#region headerAD    
   {
        code: 'top_bill',
        mediaTypes: {
            banner: {
                sizes: [[336, 280]],
            }
        }
        ,
        bids: [ 
        {
            bidder: 'adform',
            params: {
                mid: '451467'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId: 150872, pageId: 780349, formatId: 52161,
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2581, siteId: 461666, pageId: 1451617, formatId: 55373,
            }
        } 
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '386578',
                    zoneId:'2153596',
                    sizes:'15',
            }
        }
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1418175,
                publisherSubId:'top_bill'
            }
        }
        ,
    
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        } 
        ,
        {
            bidder: 'sspBC'
        }  
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 22783799
            }
        } 
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1053218'
            }
        }
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'faktopedia-pl',
                    adUnitElementId: 'top_bill',
                    placement: 'top_bill',
                    environment: 'mobile',
                    }
                }

        ]
    
    }  
    ,
    {
        code: 'middle_rect',
        mediaTypes: {
            banner: {
                sizes: [[336, 280]],
            }
        }
        ,
        bids: [ {
            bidder: 'adform',
            params: {
                mid: '1136704'
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2422, siteId:  150872, pageId: 780350, formatId: 52161,
            }
        }
        ,
        {
            bidder: 'smartadserver',
            params: {
                networkId: 2581, siteId:  461666, pageId: 1472298, formatId: 54303,
            }
        }
        ,
        {
            bidder: 'sspBC'
        } 
        ,
        {
            bidder: 'rubicon',
                params: {
                    accountId: '21594',
                    siteId: '386578',
                    zoneId:'2153596',
                    sizes:'15',
            }
        }  
        ,
        {
            bidder: 'criteo',
            params: {
                zoneId: 1418175,
                publisherSubId:'middle_rect'
            }
        } 
        ,
    
        {
            bidder: "rtbhouse",
            params: {
                region: 'prebid-eu',
                publisherId: 'ubZbp6DokIAsAJBBqd3T'
            }
        } 
        ,
        {
            bidder: 'oftmedia',
            params: {
                placementId: 22783799
            }
        }
        ,
        {
            bidder: 'connectad',
            params: {
               networkId: '10047',
               siteId: '1053218'
            }
        }
        ,
        {
            bidder: 'adagio',
            params:{
                    organizationId: '1120',
                    site: 'faktopedia-pl',
                    adUnitElementId: 'middle_rect',
                    placement: 'middle_rect',
                    environment: 'mobile',
                    }
                }
        ]
    } 
        ,
        {
            code: 'bottom_rect',
            mediaTypes: {
                banner: {
                    sizes: [[336, 280]],
                }
        }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '1136705'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId:  150872, pageId: 780351, formatId: 52161,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId:  461666, pageId: 1472299, formatId: 54304,
                }
            }
            ,
            {
                bidder: 'sspBC'
            } 
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '386578',
                        zoneId:'2153594',
                        sizes:'15',
                }
            }
            ,
            {
                bidder: 'criteo',
                params: {
                    zoneId: 1418180,
                    publisherSubId:'bottom_rect'
                }
            } 
            ,
    
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu',
                    publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            }  
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 22783799
                }
            }
            ,
            {
                bidder: 'connectad',
                params: {
                   networkId: '10047',
                   siteId: '1053218'
                }
            }
            ,
            {
                bidder: 'adagio',
                params:{
                        organizationId: '1120',
                        site: 'faktopedia-pl',
                        adUnitElementId: 'bottom_rect',
                        placement: 'bottom_rect',
                        environment: 'mobile',
                        }
                    }
                 
        
        ]
    }          
    
    ];
//#endregion
//#region waluty, przeliczenia, buckety
var pbjs=pbjs || {};
pbjs.que=pbjs.que || [];
var USD=3.80;
var EUR=4.50;
pbjs.bidderSettings= {
    criteo: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*EUR;
        }
    }
    ,
    adform: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.9)*EUR;
        }
    }
    ,
    smartadserver: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm;
        }
    }
    ,
    rtbhouse: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    pulsepoint: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    emx_digital: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    onedisplay: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.85)*EUR;
        }
    }
    ,
    oftmedia: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.85)*USD;
        }
    }
    ,
    imonomy: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    rubicon: {
        bidCpmAdjustment: function(bidCpm) {
            return (bidCpm*0.75)*USD;
        }
    }
    ,
    ix: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    connectad: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm*USD;
        }
    }
    ,
    amx: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm;
        }
    }
    ,
    visx: {
        bidCpmAdjustment: function(bidCpm) {
            return bidCpm;
        }
    }
    ,
    standard: {
        adserverTargeting: [ {
            key: "hb_bidder",
            val: function (bidResponse) {
                return bidResponse.bidderCode;
            }
        }
        ,
        {
            key: "hb_adid",
            val: function (bidResponse) {
                return bidResponse.adId;
            }
        }
        ,
        {
            key: "hb_pb",
            val: function(bidResponse) {
                var cpm=bidResponse.cpm;
                if (cpm < 10.00) {
                    return (Math.floor(cpm * 100) / 100).toFixed(2);
                }
                else {
                    return '10.00';
                }
            }
        }
        ]
    }
};